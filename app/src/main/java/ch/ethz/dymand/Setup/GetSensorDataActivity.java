package ch.ethz.dymand.Setup;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;

import ch.ethz.dymand.Audio.AudioRecorder;
import ch.ethz.dymand.Audio.WavRecorder;
import ch.ethz.dymand.FGService;
import ch.ethz.dymand.R;
import ch.ethz.dymand.UserControlledDataCollection;

import static ch.ethz.dymand.Config.getDateNowForFilename;
import static ch.ethz.dymand.Config.hasVoiceSampleBeenCollected;
import static ch.ethz.dymand.Config.isSetupComplete;
import static ch.ethz.dymand.Config.subjectID;

public class GetSensorDataActivity extends WearableActivity {

    private Button recordButton;
    private boolean isRecording = false;
    private String TAG = "GetVoiceSampleActivity";
    private AudioRecorder recorder;
    private UserControlledDataCollection sensorData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_sensor_data);

        // Enables Always-on
        setAmbientEnabled();


        sensorData = UserControlledDataCollection.getInstance(this);

        Log.i(TAG, "Getting voice sample");
        recordButton = findViewById(R.id.recordBtn);

        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isRecording) {
                    recordButton.setText("Finishing...");
                    recordButton.setEnabled(false);
                    isRecording = false;
                    stopRecording();
                    recordButton.setEnabled(false);
                } else {
                    isRecording = true;
                    recordButton.setText("Stop Recording");
                    startRecording();
                }

            }
        });
    }


    public void startRecording(){
//        Log.d(TAG, "starting recording");
//        long timeStamp = System.currentTimeMillis();
//
//        Calendar cal = Calendar.getInstance();
//        int day = cal.get(Calendar.DAY_OF_WEEK)-1;
//        int hour = cal.get(Calendar.HOUR_OF_DAY);
//
//        String dirPath = getApplicationContext().getExternalFilesDir(null).getAbsolutePath()+"/Subject_" + subjectID +
//                "/Voice_Sample/" +"/Day_" + day + "/Hour_" + hour + "/" + getDateNowForFilename() + "/";
//        File dir = new File(dirPath);
//        Log.i(TAG, "directory exist: " + dir.exists());
//        dir.mkdirs();
//        recorder.startRecording(dirPath);
        //startService();
        recorder = new AudioRecorder(GetSensorDataActivity.this);
        sensorData.startRecording();
    }

    public void stopRecording(){
//        Log.d(TAG, "stopping recording");
//        recorder.stopRecording();

        sensorData.stopRecording();
        scheduleTransition();
    }

    public void completeRecording(){

        //Kill service
        //stopService();

        //Finish activity
//        this.finish();
//        android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(1);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    public void scheduleTransition(){
        //Create timer using handler and runnable
        final Handler timerHandler = new Handler();

        Runnable timerRunnable = new Runnable() {
            @Override
            public void run() {
                completeRecording();
            }
        };

        timerHandler.postDelayed(timerRunnable, 5000);
    }

    private void stopService(){
        Intent mService = new Intent(this, FGService.class);
        this.stopService(mService);
    }

    private void startService(){
        if(isMyServiceRunning(FGService.class)) {
            Toast.makeText(this, "Service exists. Kill it before starting a new one...", Toast.LENGTH_SHORT).show();
            return;
        }

        FGService.acquireStaticLock(this);
        Intent mService = new Intent(this, FGService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(mService);
        }
        else{

            startService(mService);
        }

        Toast.makeText(this, "Starting service: ", Toast.LENGTH_SHORT).show();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
