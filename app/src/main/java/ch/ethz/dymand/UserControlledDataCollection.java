package ch.ethz.dymand;

import android.content.Context;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;

import ch.ethz.dymand.Audio.AudioRecorder;
import ch.ethz.dymand.Sensors.SensorRecorder;

import static android.content.Context.VIBRATOR_SERVICE;
import static ch.ethz.dymand.Config.DEBUG_MODE;
import static ch.ethz.dymand.Config.dataCollectEndDate;
import static ch.ethz.dymand.Config.dataCollectStartDate;
import static ch.ethz.dymand.Config.getDateNow;
import static ch.ethz.dymand.Config.getDateNowForFilename;
import static ch.ethz.dymand.Config.recordingTriggeredDates;
import static ch.ethz.dymand.Config.recordingTriggeredNum;
import static ch.ethz.dymand.Config.subjectID;

public class UserControlledDataCollection{
    private AudioRecorder audioRecorder;
    SensorRecorder mSensorRecorder;
    private  static UserControlledDataCollection instance = null;

    private static final String LOG_TAG = "User Controlled Data Collection";
    private Callbacks.WatchPhoneCommCallback commCallback;

    private static Context context;
    private static String dirPath;

    public static UserControlledDataCollection getInstance(Context contxt) {
        if (instance == null) {
            instance = new UserControlledDataCollection();
            context = contxt;
        }
        return instance;
    }

    public void startRecording(){
        //Log that recording has started
        recordingTriggeredNum++;
        recordingTriggeredDates = recordingTriggeredDates + getDateNow();

        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_WEEK)-1;
        int hour = cal.get(Calendar.HOUR_OF_DAY);

        dirPath = context.getExternalFilesDir(null).getAbsolutePath()+"/Subject_" + subjectID +
                "/Day_" + day + "/Hour_" + hour + "/" + getDateNowForFilename() + "/";

        File dir = new File(dirPath);
        Log.i(LOG_TAG, "directory exist: " + dir.exists());
        dir.mkdirs();


        audioRecorder = new AudioRecorder(context);
        mSensorRecorder = new SensorRecorder(context);
        mSensorRecorder.startRecording(dirPath); // non blocking return.
        audioRecorder.startRecording(dirPath);

        //Record start of data collection
        dataCollectStartDate = dataCollectStartDate + getDateNow();


        if (DEBUG_MODE == true){
            String displayMsg = "Recording started";
            Toast.makeText(context, displayMsg, Toast.LENGTH_LONG).show();
            Vibrator v = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
            v.vibrate(500); // Vibrate for 500 milliseconds
        }
    }

    /**
     * Stops recording
     */
    public void stopRecording() {
        mSensorRecorder.stopRecording();
        audioRecorder.stopRecording();

        //Record end of data collection
        dataCollectEndDate = dataCollectEndDate + getDateNow();

        if (DEBUG_MODE == true){
            String displayMsg = "Recording stopped";
            Toast.makeText(context, displayMsg, Toast.LENGTH_LONG).show();
            Vibrator v = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
            v.vibrate(500); // Vibrate for 500 milliseconds
        }
    }

}
