package ch.ethz.dymand;

import java.io.FileNotFoundException;

public interface Callbacks {

    interface DataCollectionCallback{
         void collectDataCallBack() throws FileNotFoundException;
    }


    interface BleCallback{
        void startBleCallback();

        void stopBleCallback();

        void reStartBleCallback();
    }

    interface WatchPhoneCommCallback{
        void signalPhone();
    }
}
